# 大翻译运动 (The Great Translation Movement)

这是[大翻译运动](https://zh.wikipedia.org/wiki/%E5%A4%A7%E7%BF%BB%E8%AF%91%E8%BF%90%E5%8A%A8)的非官方项目库。建立该库的初衷是为了将分布式的协作发挥得更加有效。该项目库的管理员非TGTM的官方成员，而是其中一位匿名参与者。该库的首要目的是：

 * 接受投稿
 * 协调翻译
 * 自由发布

This is a project for the Great Translation Movement. This is created by one participant to coordinate the decentralized movement. It is NOT official. 

## 参与 (Getting started)

参与该项目需要注册gitlab帐号，该帐号只需要一个邮箱即可。可以申请[protonmail](https://protonmail.com/)来确保隐私。 示例见[这里](https://gitlab.com/tgtm/the-great-translation-movement/-/issues/1)

### 投稿

通过创建问题[Creating an issue](https://gitlab.com/tgtm/the-great-translation-movement/-/issues/new) ,来提交素材。

投稿请输入相应关键字，以便后来的投稿人查询以防重复：

 * 报道的ID/平台，如腾讯新闻
 * 内容关键字
 * 新闻类别，如*疫情*，*战争*，*人权*等

### 翻译

翻译文件可以通过回复该问题进行提交。

### 发布

任何人可以自由发布所提交的内容，并请将发布的链接回复到问题系列中。


#### Twitter 

如果在推特（twitter）上发布内容，请按照一下格式：

 1. 添加标签 (根据语言选取)

 #大翻译运动 
 #TheGreatTranslationMovement
 #偉大な翻訳運動 
 #LaGrandeTraduction

 2. At popular media account. 

### 整理 (Archive)

该项目成员不定期将所翻译的文件，整理入库。欢迎克隆该项目内容，并进行分发。

## Roadmap 

如果本项目受到欢迎，导致内容太多，欢迎志愿者加入项目团队进行内容整理。


